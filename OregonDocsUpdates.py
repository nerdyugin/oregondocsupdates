#!/usr/local/bin/python3
from bs4 import BeautifulSoup 
import os
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import time
import datetime

#driver = webdriver.Chrome(r"/Users/MacDev/Documents/projects/kindred/kindred-pyscripts/chromedriver")
#"C:\Users\HPowerTiny\Documents\Projects\Kindred\chromedriver.exe")
#driver.get("http://library.state.or.us/home/techserv/ordocs/?shiplist=2018-12")

def main(sourceLink,mac = False):
    #environment variables that need to be set
    user = os.environ["ALMA_USER"]
    password = os.environ["ALMA_PASS"]
    driverLocation = os.environ["ALMA_CHROME_DRIVER"]

    driver = webdriver.Chrome(driverLocation)
    driver.get(sourceLink)
    soup = BeautifulSoup(driver.page_source,features="html.parser")
    listOfFoundObjects = soup.find_all('font', color="red")

    UpdatesToAlma = []

    for foundObject in listOfFoundObjects:
        Set = foundObject.contents[0].replace('\n','').replace('Note: ','')
        parsedItems = Set.split(',')
        linkIndex = len(parsedItems) - 1
        update = []
        link = parsedItems[linkIndex].replace(" ",'')
        update.append(link)
        for item in parsedItems:
            if 'OCLC #' in item:
                oclc = item.replace("OCLC #", "").replace(" ","")
                if oclc == '':
                    continue
                else:
                    update.append(oclc)
        UpdatesToAlma.append(update)

    # Web driver that is needed for selenium to run
    driver = webdriver.Chrome(r"/Users/MacDev/Documents/projects/kindred/kindred-pyscripts/chromedriver") #mac
    #driver = webdriver.Chrome("C:\Users\HPowerTiny\Documents\Projects\Kindred\chromedriver.exe") #windows
    driver.get("https://na01.alma.exlibrisgroup.com/mng/login?institute=01ALLIANCE_OIT&auth=local")

    # The use of the fullscreen/maximize windwo is for some objects to be visible by selenium 
    if mac:
        driver.fullscreen_window() #for mac
    else:
        driver.maximize_window() #for windows

    # ---------------------- Alma Login -----------------------
    actions = ActionChains(driver)
    actions2 = ActionChains(driver)
    actions.move_to_element(driver.find_element_by_name('username')).click().send_keys(user)\
        .move_to_element(driver.find_element_by_name('password')).click().send_keys(password).perform()
    actions2.move_to_element(driver.find_element_by_class_name('loginSubmit')).click().perform()
    driver.implicitly_wait(30)
    # ---------------------------------------------------------

    element_id_barcodeSearch_id = "ALMA_MENU_TOP_NAV_Search_Text"
    element_xpath_view = """//*[@id="SELENIUM_ID_results_0_inventoryLookAheadelectronicItems_ROW_0_COL_simpleViewLabel"]/a"""
    element_xpath_linking = """//*[@id="cresource_editorportfolio_tablinkingInformation_span"]/a"""
    element_xpath_linkTexBox = """//*[@id="pageBeanlocalParseParam"]"""
    element_xpath_saveButton = """//*[@id="PAGE_BUTTONS_cbuttonsave"]"""

    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d(%H.%M.%S)')
    filename = f'errored_oclc_{timestamp}.txt'
    file = open(filename,"w")

    stopcheck = ''
    for update in UpdatesToAlma:
        print(update)
        link = update[0]
        for oclc in update:
            if oclc != link:
                print(oclc)
                keepRunning = True
                successful = False
                waitAfterSearch = 2
                waitForPageToRender = 0.5
                waitAfterSaveButton = 2
                while keepRunning:
                    try:
                        if waitAfterSearch < 15:
                            driver.find_element_by_id(element_id_barcodeSearch_id).send_keys(oclc)
                            driver.find_element_by_id(element_id_barcodeSearch_id).send_keys(Keys.RETURN)
                            time.sleep(waitAfterSearch)
                            # ------------------ Find "View" Protfolio Link ----------------------
                            foundViewLink = False
                            findViewLinkAttempt = 0
                            while not foundViewLink:
                                try:
                                    time.sleep(waitForPageToRender)
                                    foundViewLink = driver.find_element_by_xpath(element_xpath_view)
                                    foundViewLink.click()
                                    time.sleep(waitForPageToRender)
                                except:
                                    print("trying to find View Link: "+findViewLinkAttempt)
                                    time.sleep(1)
                                    if findViewLinkAttempt > 5:
                                        break
                            
                            if foundViewLink:
                                # -------------- Updating the Protfolio Link ---------------------
                                driver.find_element_by_xpath(element_xpath_linking).click()
                                time.sleep(waitForPageToRender)
                                # Additng the new link
                                driver.find_element_by_xpath(element_xpath_linkTexBox).clear()
                                driver.find_element_by_xpath(element_xpath_linkTexBox).send_keys(link)
                                driver.find_element_by_xpath(element_xpath_saveButton).click()
                                time.sleep(waitAfterSaveButton)
                                keepRunning = False
                                successful = True
                            else:
                                keepRunning = False
                                successful = False

                        else:
                            keepRunning = False
                    except:
                        print("error occured")
                        waitAfterSearch += 5
                        waitForPageToRender += 5
                        waitAfterSaveButton += 5
                        continue
                    finally:
                        driver.find_element_by_id(element_id_barcodeSearch_id).clear()
                if not successful:
                    file.write(oclc + ", "+link)

    file.close()
    driver.quit()

if __name__ == '__main__':
    mac = False
    promptFromUser = input('Are you on a mac? If yes, type "yes" for yes, otherwise enter anything: ')
    if "yes" in promptFromUser or \
        "Yes" in promptFromUser or \
        "YES" in promptFromUser or "y" in promptFromUser:
        mac = True
    sourceLink = input("Enter the url from the state, then press Enter: ")
    print("Environment variables that must be set: ALMA_USER, ALMA_PASS, ALMA_CHROME_DRIVER")
    promptFromUser = ('Environment user and password set? Type "yes" for yes: ')
    if "yes" in promptFromUser or \
        "Yes" in promptFromUser or \
        "YES" in promptFromUser or "y" in promptFromUser:
        main(sourceLink,mac)

""" psudo code (original)
https://www.oregon.gov/Library/collections/Pages/State-Government-Publications-Librarians.aspx

#--------------------------------------------------
//*[@id="bfc9a2fc-9acd-48ec-9581-109cc9bd6dab"]/div/div[2]/div/p[2]/a 
<a href="http://library.state.or.us/home/techserv/ordocs/?shiplist=2018-12" data-or-analytics-event-attached="true">2018-12</a>

report back if shipping list doesn't exist for a month.

#-------------------------------------------------
It has to be "Note: OCLC "
Note: OCLC #1013887930, https://digital.osl.state.or.us/islandora/object/osl:495815

If its been re-catolloged then write to excel file.

store the changes into an array (list)
#--------------------------------------------------
Go to alma
https://na01.alma.exlibrisgroup.com/mng/login?institute=01ALLIANCE_OIT&auth=local

login

paste the OCLC for every one thats in the list of the record (meaning multiple OCLC)
then search
then look for '//*[@id="SELENIUM_ID_results_0_inventoryLookAheadelectronicItems_ROW_0_COL_packageNameUI"]/a'
(Oregon Docs Link)

Then click on view, then click on linking, then paste the link, then save.
"""

